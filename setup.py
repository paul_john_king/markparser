# coding: utf-8

# markparser - A command-line parser that outputs help in Markdown format
# Copyright (c) 2020  Paul John King (p.king@openinfrastructure.de)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License v3.0 as published by the
# Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from ast import(
	Assign, Expression,
	fix_missing_locations, get_docstring, iter_child_nodes, parse
)
from io import(
	open
)
from inspect import(
	cleandoc
)
from os import(
	environ, walk
)
from os.path import(
	join, sep
)
from re import(
	compile as compile_re
)
from setuptools.extern.packaging.version import(
	Version
)
from setuptools import(
	find_packages, setup
)
from subprocess import(
	STDOUT,
	CalledProcessError,
	check_output
)
from sys import(
	version_info
)


def indent(text):
	return "\n".join(
		"    %s"%(line)
		for line
		in text.strip().splitlines()
	)

def get_package():
	try:
		return environ["PACKAGE_NAME"]
	except:
		raise RuntimeError("The environment variable `PACKAGE_NAME` is not set.")

def get_python():
	return "python%(major)d.%(minor)d"%{
		"major": version_info.major,
		"minor": version_info.minor
	}

def get_dunders(package):
	dunders = {}
	path = join(package, "__init__.py")
	with open(path, "rb") as file:
		source = file.read()
	root_node = parse(
		source=source,
		filename=path,
		mode="exec"
	)
	for node in iter_child_nodes(root_node):
		if isinstance(node, Assign):
			expr = Expression(node.value)
			expr = fix_missing_locations(expr)
			code = compile(
				source=expr,
				filename=path,
				mode="eval"
			)
			try:
				value = eval(code)
				for target in node.targets:
					name = target.id
					if (
						name.startswith("__")
						and name.endswith("__")
					):
						dunders[name[2:-2]] = value
			except:
				pass
	if "doc" in dunders:
		dunders["doc"] = cleandoc(dunders["doc"])
	else:
		dunders["doc"] = get_docstring(root_node)
	return dunders

def get_version(prefix="version_"):
	try:
		output = check_output(
			args=["git", "describe", "--match", "%s*"%(prefix), "--long"],
			stderr=STDOUT
		).decode("utf-8").strip().split("-")
		tag = output[0][len(prefix):]
		pre = "-".join(output[1:-2])
		patch = output[-2]
		commit = output[-1]
		if pre:
			version = "%(tag)s.0-%(pre)s.%(patch)s+%(commit)s"%{
				"tag": tag,
				"pre": pre,
				"patch": patch,
				"commit": commit
			}
		else:
			version = "%(tag)s.%(patch)s+%(commit)s"%{
				"tag": tag,
				"patch": patch,
				"commit": commit
			}
		return str(Version(version))
	except CalledProcessError as exception:
		raise RuntimeError(
			"Cannot extract Git description because\n%(cause)s"%{
				"cause": indent(exception.output.decode("utf-8"))
			}
		)
	except Exception as exception:
		raise RuntimeError(
			"Cannot extract Git description because\n%(cause)s"%{
				"cause": indent(str(exception))
			}
		)

def find_data_files(path):
	data_files = []
	for path, dir_name, file_names in walk(path):
		if file_names:
			target_path = "%(root)s%(rest)s"%{
				"root": sep,
				"rest": sep.join(path.split(sep)[1:])
			}
			source_paths = [
				join(path, file_name)
				for file_name
				in file_names
			]
			data_files.append((target_path, source_paths))
	return data_files

def ammend_file(package, version):
	path = join(package, "__init__.py")
	pattern = compile_re(r"^(\s*__version__\s*=\s*)(.*)")
	replacement = r'\g<1>"%s"'%(version)
	with open(path, "r", encoding="utf-8") as file:
		lines = [
			pattern.sub(replacement, line)
			for line
			in file
		]
	with open(path, "w", encoding="utf-8") as file:
		for line in lines:
			file.write(line)


if __name__ == "__main__":
	try:
		package = get_package()
		python = get_python()
		dunders = get_dunders(
			package=package
		)

		name = "%(python)s-%(package)s"%{
			"python": python,
			"package": package
		}
		version = get_version()
		author = dunders.get("author", "")
		author_email = dunders.get("email", "")
		url = dunders.get("uri", "")
		license = dunders.get("license", "")
		description = dunders.get("doc", "\n").splitlines()[0]
		long_description = dunders.get("doc", "")
		python_requires = dunders.get("python_requires", "")
		classifiers = dunders.get("classifiers",[])
		packages = find_packages()
		data_files = find_data_files(
			path="includes"
		)

		try:
			ammend_file(
				package=package,
				version=version
			)
			setup(
				name=name,
				version=version,
				author=author,
				author_email=author_email,
				url=url,
				license=license,
				description=description,
				long_description=long_description,
				python_requires=python_requires,
				classifiers=classifiers,
				packages=packages,
				data_files=data_files
			)
		finally:
			ammend_file(
				package=package,
				version="?"
			)
	except Exception as exception:
		exit(
			"Error: %(cause)s"%{
				"cause": str(exception)
			}
		)
