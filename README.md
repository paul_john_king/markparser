<!--
The script `project` generates this file from the project's source code, and
will overwrite changes made here.  In order to change this file permanently,
edit the project's source code, and call `project document`.
-->

A command-line parser that outputs help in Markdown format

This package provides subclasses of the classes `ArgumentParser` and `Action`
of the Python package `argparse` that output help and usage in Markdown format.

This project contains the sources of the package, and tools to document, build,
deploy and clean the project.

Project Contents
================

The directory at the path `markparser` contains the sources of the Python
package `markparser`.

The Bourne shell script at the path `project` has commands to document,
build, deploy and clean the project.

*   The `document` command writes a description of the project in [GitLab
    Flavoured Markdown][gitlab-markdown] format to the file at the path
    `README.md`.

*   The `build` command uses the Python script at the path
    `setup.py` to produce a versioned RPM package containing a
    versioned Python packeage of `markparser` and the contents of the
    directory at the path `includes`.

[gitlab-markdown]: https;//docs.gitlab.com/ee/user/markdown.html

Package `markparser`
====================

A command-line parser that outputs help in Markdown format

This package provides subclasses of the classes `ArgumentParser` and `Action`
of the Python package `argparse` that output help and usage in Markdown format.

*   Version: ?
*   URI:     https://gitlab.openinfrastructure.de/openi/python.markparser
*   Author:  Paul John King <p.king@openinfrastructure.de>
*   License: LGPLv3
*   Python:  >=2.7
*   Classifiers:
    *   Development Status :: 5 - Production/Stable
    *   License :: OSI Approved :: GNU Lesser General Public License v3
    *   Operating System :: OS Independent
    *   Programming Language :: Python

Script `project`
================

Usage
-----

    project usage

Write a short usage message to standard output and exit.

--------------------------------------------------------------------------------

    project help

Write a long help message to standard output and exit.

--------------------------------------------------------------------------------

    project document

Write a description of the project in Markdown format to the file at
`README.md`.

--------------------------------------------------------------------------------

    project build

For each Python interpreter in

*   `python2`
*   `python3`

build an RPM package containing the Python package for the interpreter.  Write
the package to the directory `dist`.

While building the RPM package, extract the version of the Python package from
the Git repository (see below) and insert it into the module `__init__` of the
Python package as the value of the dunder `__version__`.  A Python package
installed from the RPM package can thus now report its version.

Give the RPM package the name

    python«interpreter_version»-markparser-«package_version»

where

*   `«interpreter_version»` is the version of the Python interpreter, and

*   `«package_version»` is the version of the Python package (see below).

Including the version of the Python interpreter in the name of the RPM package
makes it possible to install the Python package for multiple Python interpreters
on a single machine via RPM.  Give the RPM package a summary of the last commit
to the Git repository as its change log.

The version of the Python package is derived from the most recent Git commit
with an annotated tag that begins `version_`.  If the annotated tag has the
form

    version_«tag»

where `«tag»` contains no character `-` then the preliminary version of the
Python package is

    «tag».«patch»+g«çommit»

but if the annotated tag has the form

    version_«tag»-«pre»

where `«tag»` contains no character `-` then the preliminary version of the
Python package is

    «tag».0-«pre».«patch»+g«çommit»

where

*   `«patch»` is the number of Git commits since the commit with the annotated
    tag, and

*   `«commit»` is the first seven characters of the ID of the current Git
    commit.

The definitive version of the Python package is the preliminary version
normalised in accord with [section "Normalization" of "PEP 440 – Version
Identification and Dependency Specification"][pep440-normalization].

--------------------------------------------------------------------------------

    project deploy

Copy the contents of the directory `dist` to the directory

    «deployment_root»/«ci_project_path»/«ci_commit_ref_name»

on the local host, where

*   `«deployment_root»` is the value of the environment variable
    `DEPLOYMENT_ROOT`,

*   `«ci_project_path»` is the value of the environment variable
    `CI_PROJECT_PATH`, and

*   `«ci_commit_ref_name»` is the value of the environment variable
    `CI_COMMIT_REF_NAME`.

[The "Predefined environment variables reference"][gitlab-variables] states that
GitLab CI predefines the values of

*   `CI_PROJECT_PATH` to the "namespace with project name", and

*   `CI_COMMIT_REF_NAME` to the "branch or tag name for which project is
    built".

Thus, if a GitLab server hosts the project such that

*   the GitLab project sets the environment variable `DEPLOYMENT_ROOT` to
    `/srv/artifacts` (in the section "Environment variables" of the page
    "Setting » CI/CD" of the GitLab project),

*   the GitLab project is in the group ${_GITLAB_GROUP}`,

*   the GitLab project has the name `{_GITLAB_NAME}`,

*   the Git branch is `version/4`, and

*   the GitLab-CI script calls `project deploy`

then GitLab CI

*   sets the value of `CI_PROJECT_PATH` to `openi/python.markparser`,

*   sets the value of `CI_COMMIT_REF_NAME` to `version/4`, and

*   copies the contents of the directory `dist` to the directory

        /srv/artifacts/openi/python.markparser/version/4

    on the GitLab-CI runner host.

--------------------------------------------------------------------------------

    project clean

Delete each file created when building, deploying or running the package.  These
files are

*   the directories `build` and `dist`, and

*   each file in the project with the name `__pycache__` or the suffix
    `.egg_info` or `.pyc`.

[gitlab-variables]: https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
[pep440-normalization]: https://www.python.org/dev/pep-0440/#normalization]

Warning
=======

The script `project` generates this file from the project's source code, and
will overwrite changes made here.  In order to change this file permanently,
edit the project's source code, and call `project document`.
