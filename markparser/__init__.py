# coding: utf-8

# markparser - A command-line parser that outputs help in Markdown format
# Copyright (c) 2020  Paul John King (p.king@openinfrastructure.de)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License v3.0 as published by the
# Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""\
A command-line parser that outputs help in Markdown format

This package provides subclasses of the classes `ArgumentParser` and `Action`
of the Python package `argparse` that output help and usage in Markdown format.
"""

__version__ = "?"
__uri__ = "https://gitlab.openinfrastructure.de/openi/python.markparser"
__author__ = "Paul John King"
__email__ = "p.king@openinfrastructure.de"
__license__ = "LGPLv3"
__python_requires__ = ">=2.7"
__classifiers__ = [
	"Development Status :: 5 - Production/Stable",
	"License :: OSI Approved :: GNU Lesser General Public License v3",
	"Operating System :: OS Independent",
	"Programming Language :: Python"
]
__info__ = """
*   Version: %(version)s
*   URI:     %(uri)s
*   Author:  %(author)s <%(email)s>
*   License: %(license)s
*   Python:  %(python_requires)s
*   Classifiers:
%(classifiers)s
""".strip()%{
	"version": __version__,
	"uri": __uri__,
	"author": __author__,
	"email": __email__,
	"license": __license__,
	"python_requires": __python_requires__,
	"classifiers": "\n".join(
		"    *   %s"%(classifier)
		for classifier
		in __classifiers__
	)
}

from .markparser import(
	MarkdownArgumentParser, UsageAction
)
